<!DOCTYPE html>
<html>
	<head>
		<title> Nibble Front-End Test </title>
		
		<!-- BOOTSTRAP 4.0 -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/override.css">

		<!-- FREE FONT GOOGLE ON CAROUSEL -->
		<link href="https://fonts.googleapis.com/css?family=Luckiest+Guy&display=swap" rel="stylesheet">

		<!-- CONTROL CAROUSEL WITH MOUSE DRAG -->
		<link rel="stylesheet" type="text/css" href="css/slick.css">
	</head>

	<body>
		<nav class="navbar navbar-expand-lg">
			<!-- LOGO -->
			<a class="navbar-brand" href="#">
				<img src="img/logo/logo.png" width="40" height="40" alt="">
			</a>

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    			<span class="navbar-toggler-icon">
    				<img style="width: 100%; height:100%" alt="svgImg" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHg9IjBweCIgeT0iMHB4Igp3aWR0aD0iNTAiIGhlaWdodD0iNTAiCnZpZXdCb3g9IjAgMCAyMjYgMjI2IgpzdHlsZT0iIGZpbGw6IzAwMDAwMDsiPjxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0ibm9uemVybyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIHN0cm9rZS1saW5lY2FwPSJidXR0IiBzdHJva2UtbGluZWpvaW49Im1pdGVyIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHN0cm9rZS1kYXNoYXJyYXk9IiIgc3Ryb2tlLWRhc2hvZmZzZXQ9IjAiIGZvbnQtZmFtaWx5PSJub25lIiBmb250LXdlaWdodD0ibm9uZSIgZm9udC1zaXplPSJub25lIiB0ZXh0LWFuY2hvcj0ibm9uZSIgc3R5bGU9Im1peC1ibGVuZC1tb2RlOiBub3JtYWwiPjxwYXRoIGQ9Ik0wLDIyNnYtMjI2aDIyNnYyMjZ6IiBmaWxsPSJub25lIj48L3BhdGg+PGcgZmlsbD0iI2ZmZmZmZiI+PHBhdGggZD0iTTAsMzMuOXYyMi42aDIyNnYtMjIuNnpNMCwxMDEuN3YyMi42aDIyNnYtMjIuNnpNMCwxNjkuNXYyMi42aDIyNnYtMjIuNnoiPjwvcGF0aD48L2c+PC9nPjwvc3ZnPg==">
    			</span>

  			</button>

			<!-- MID MENU -->	
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<div class="mx-auto order-0">
					<ul class="navbar-nav mr-auto">
						<?php 
							include "database.php";

							while ($row = mysqli_fetch_array($result)) {
								if ($row["dropdown"] == 1) {
									echo "
										<li class='nav-item dropdown'>
											<a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>".$row["name"]."</a>
											<div class='dropdown-menu' aria-labelledby='navbarDropdown'>
												<a class='dropdown-item' href='#'>blog</a>
												<a class='dropdown-item' href='#'>single-blog</a>
											</div>
										</li>";
								} else {
									echo "
										<li class='nav-item'>							
											<a class='nav-link' href='#''>".$row["name"]."</a></li>";
								}
							} 
						?>
						
					</ul> 
    			</div>
				<button type="button" class="btn btn-danger" id="join_button">JOIN US</button>
			</div>
		</nav>

		<!-- CAROUSEL -->
		<div id="carouselExampleControls" class="carousel slide">
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img class="d-block w-100" src="img/fit.jpg" alt="First slide">
					<div class="carousel-caption d-md-block">
						<p class="font-banner" style="font-size: 7vw">BUILD UP YOUR</p>
						<p class="font-banner" style="font-size: 12vw">STRENGTH</p>
						<p style="font-size: 2vw">Build Your Body and Fitness with Professional Touch</p>
						<button type="button" class="btn btn-danger carousel-join-btn">JOIN US</button>
					</div>
				</div>
				<div class="carousel-item">
					<div class="carousel-caption d-md-block">
						<p class="font-banner" style="font-size: 7vw">BUILD UP YOUR</p>
						<p class="font-banner" style="font-size: 12vw">STRENGTH</p>
						<p style="font-size: 2vw">Build Your Body and Fitness with Professional Touch</p>
						<button type="button" class="btn btn-danger carousel-join-btn">JOIN US</button>
					</div>
					<img class="d-block w-100" src="img/fit2.jpg" alt="Second slide">
				</div>
			</div>

			<!-- CAROUSEL CONTROL -->
			<a class="carousel-control-prev prev" href="#carouselExampleControls" role="button" data-slide="next">
				<span class="carousel-control-prev-icon"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next next" href="#carouselExampleControls" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div> 
	
	<!-- SCRIPT BOOTSTRAP 4.0 -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<!-- CONTROL CAROUSEL WITH MOUSE DRAG -->
    <script type="text/javascript" src="js/slick.min.js"></script>
    <script type="text/javascript" src="js/slick-function.js"></script>

	</body>
</html>