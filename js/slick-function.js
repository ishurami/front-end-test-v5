$(function(){
	$('.carousel-inner').slick({
	  centerMode: true,
      centerPadding: '60px',
      slidesToShow: 1,
      prevArrow: $('.prev'),
      nextArrow: $('.next'),
      arrows: true
	});
})